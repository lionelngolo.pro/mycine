import { Component, OnInit, Input } from '@angular/core';
import { FilmInterface } from '../interface/filmInterface';
import { FilmService } from '../service/film.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogEditFilmComponent } from '../dialog-edit-film/dialog-edit-film.component';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-myfilms',
  templateUrl: './myfilms.component.html',
  styleUrls: ['./myfilms.component.scss'],
})
export class MyfilmsComponent implements OnInit {
  @Input() listeFilms: FilmInterface[] = [];
  apiValues: any;
  apiValuesTemp: any;
  p: number = 1;

  constructor(
    private filmService: FilmService,
    public dialog: MatDialog,
    public router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.filmService.getAllFilms().subscribe((response) => {
      this.apiValues = response;
      this.apiValuesTemp = response;
    });
  }

  openDialog(data: FilmInterface): void {
    let dialogRef = this.dialog.open(DialogEditFilmComponent, {
      data: {
        id: data.id,
        titre: data.titre,
        sypnosis: data.sypnosis,
        note: data.note,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.refreshData();
      console.log(`Dialog result: ${result}`);
    });
  }

  message(message: string): void {
    console.log(message);
  }

  popUp(message: string): void {
    alert(message);
  }

  deleteMovie(index: number): void {
    this.listeFilms.splice(index, 1);
    this.toastr.success('Supression réussie', 'Supression');
  }

  deleteMovieDynamicly(id: number): void {
    this.filmService.deleteFilm(id).subscribe((res) => {
      console.log(res);
      this.toastr.success('Supression réussie', 'Supression');
    });
    setTimeout(() => {
      this.refreshData();
    }, 2000);
  }

  refreshData(): void {
    this.filmService.getAllFilms().subscribe((response) => {
      this.apiValues = response;
    });
  }

  goToDetails(data: FilmInterface) {
    this.router.navigate(['film-detail', data.id], { state: data });
  }

  searchFilm(event: any): void {
    this.apiValues = this.apiValuesTemp.filter((item: any) =>
      item.titre.toUpperCase().includes(event.target.value.toUpperCase())
    );
  }
}
