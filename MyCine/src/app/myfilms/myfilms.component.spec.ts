import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyfilmsComponent } from './myfilms.component';

describe('MyfilmsComponent', () => {
  let component: MyfilmsComponent;
  let fixture: ComponentFixture<MyfilmsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyfilmsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyfilmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
