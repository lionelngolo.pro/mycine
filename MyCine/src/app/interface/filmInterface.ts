export interface FilmInterface {
  id?: number;
  titre: string;
  sypnosis: string;
  note: number;
}
