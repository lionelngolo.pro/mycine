export interface userInterface {
  id?: number;
  role: string;
  email: string;
  pseudo: string;
  password?: string;
}
