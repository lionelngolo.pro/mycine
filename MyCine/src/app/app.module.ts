import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddOrEditFilmComponent } from './add-or-edit-film/add-or-edit-film.component';
import { MyfilmsComponent } from './myfilms/myfilms.component';
import { FilmDetailsComponent } from './film-details/film-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { MatDialogModule } from '@angular/material/dialog';
import { DialogEditFilmComponent } from './dialog-edit-film/dialog-edit-film.component';
import { LoginComponent } from './login/login.component';
import { ToastrModule } from 'ngx-toastr';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxPaginationModule } from 'ngx-pagination';
import { ExclusiviteComponent } from './exclusivite/exclusivite.component';
import { SallesComponent } from './salles/salles.component';

@NgModule({
  declarations: [
    AppComponent,
    AddOrEditFilmComponent,
    MyfilmsComponent,
    FilmDetailsComponent,
    HeaderComponent,
    HomeComponent,
    DialogEditFilmComponent,
    LoginComponent,
    ExclusiviteComponent,
    SallesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDialogModule,
    MatTabsModule,
    BrowserAnimationsModule,
    NgxPaginationModule,
    ToastrModule.forRoot(), // ToastrModule added
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [DialogEditFilmComponent],
})
export class AppModule {}
