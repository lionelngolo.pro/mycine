import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExclusiviteComponent } from './exclusivite.component';

describe('ExclusiviteComponent', () => {
  let component: ExclusiviteComponent;
  let fixture: ComponentFixture<ExclusiviteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExclusiviteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExclusiviteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
