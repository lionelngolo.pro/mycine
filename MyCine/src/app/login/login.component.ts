import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  loginSubmit(email: string, password: string) {
    this.userService.login(email, password).subscribe((res) => {
      localStorage.setItem('logged', 'true');
      localStorage.setItem('role', res.result[0].role);
      this.authService.isLoggedIn();
      this.router.navigate(['home']);
    });
  }
}
