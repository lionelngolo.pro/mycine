import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEditFilmComponent } from './dialog-edit-film.component';

describe('DialogEditFilmComponent', () => {
  let component: DialogEditFilmComponent;
  let fixture: ComponentFixture<DialogEditFilmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogEditFilmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEditFilmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
