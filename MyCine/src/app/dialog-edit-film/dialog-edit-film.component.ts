import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormControlName, FormGroup } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { FilmService } from '../service/film.service';

@Component({
  selector: 'app-dialog-edit-film',
  templateUrl: './dialog-edit-film.component.html',
  styleUrls: ['./dialog-edit-film.component.scss'],
})
export class DialogEditFilmComponent implements OnInit {
  film: any;
  updateFilm: any = {
    id: '',
    titre: '',
    sypnosis: '',
    note: 0,
  };

  constructor(
    public dialogRef: MatDialogRef<DialogEditFilmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MatDialog,
    private filmService: FilmService,
    private toastr: ToastrService
  ) {
    console.log(data);
    this.film = data;
    this.updateFilm.id = this.film.id;
    this.updateFilm.titre = this.film.titre;
    this.updateFilm.sypnosis = this.film.sypnosis;
    this.updateFilm.note = this.film.note;
  }

  ngOnInit(): void {}

  updateForms(): void {
    console.log(this.updateFilm);
    this.filmService.updateFilm(this.updateFilm).subscribe((res) => {
      this.toastr.success('Modification réussie', 'Modification');
      this.closeDialog();
    });
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  titleInput(event: any): void {
    this.updateFilm.titre = event.target.value;
    console.log(this.updateFilm);
  }

  sypnosisInput(event: any): void {
    this.updateFilm.sypnosis = event.target.value;
    console.log(this.updateFilm);
  }

  noteInput(event: any) {
    this.updateFilm.note = event.target.value;
    console.log(this.updateFilm);
  }

  refreshData(): void {
    this.filmService.getAllFilms().subscribe((res) => {
      console.log(res);
    });
  }
}
