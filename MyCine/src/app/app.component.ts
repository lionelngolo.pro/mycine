import { Component } from '@angular/core';
import { AuthService } from './service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'MyCine';
  films: any = [];
  constructor(private authService: AuthService) {}

  addFilm(newFilm: any) {
    this.films.push(newFilm);
  }

  isLoggedIn(): Boolean {
    return this.authService.isLoggedIn();
  }
}
