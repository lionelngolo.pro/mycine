import { Component, OnInit, ViewChild } from '@angular/core';
import { throwMatDialogContentAlreadyAttachedError } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { FilmInterface } from '../interface/filmInterface';
import { MyfilmsComponent } from '../myfilms/myfilms.component';
import { FilmService } from '../service/film.service';

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.component.html',
  styleUrls: ['./film-details.component.scss'],
})
export class FilmDetailsComponent implements OnInit {
  data?: FilmInterface;

  constructor(
    private router: Router,
    private filmService: FilmService,
    private activatedRoute: ActivatedRoute
  ) {
    console.log(this.activatedRoute.snapshot.params.id);
    this.filmService
      .getFilm(this.activatedRoute.snapshot.params.id)
      .subscribe((res) => {
        this.data = res.result[0];
      });
  }

  ngOnInit(): void {}

  goBack(): void {
    this.router.navigate(['my-films']);
  }
}
