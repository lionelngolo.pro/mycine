import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  accesTrue: boolean = false;
  constructor(private authService: AuthService, private router: Router) {
    console.log(localStorage.getItem('role'));
    if (localStorage.getItem('role') == 'admin') {
      this.accesTrue = true;
    }
  }

  ngOnInit(): void {}

  LogOut(): void {
    localStorage.setItem('logged', 'false');
    this.authService.setLogged(false);
    localStorage.clear();
    this.router.navigate(['login']);
  }
}
