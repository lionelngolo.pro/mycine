import { Component, OnInit } from '@angular/core';
import { FilmInterface } from '../interface/filmInterface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  films: FilmInterface[] = [];
  constructor() {}

  ngOnInit(): void {}

  addFilm(newFilm: any) {
    this.films.push(newFilm);
  }
}
