import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { FilmInterface } from '../interface/filmInterface';
import { FilmService } from '../service/film.service';

@Component({
  selector: 'app-add-or-edit-film',
  templateUrl: './add-or-edit-film.component.html',
  styleUrls: ['./add-or-edit-film.component.scss'],
})
export class AddOrEditFilmComponent implements OnInit {
  @Output() onFilmsFormsChange = new EventEmitter<FormGroup>();

  filmsForms = new FormGroup({
    titre: new FormControl('', [Validators.required]),
    sypnosis: new FormControl('', [Validators.required]),
    note: new FormControl(null, [Validators.min(0), Validators.max(5)]),
  });

  filmsFormsDynamic = new FormGroup({
    titre: new FormControl('', [Validators.required]),
    sypnosis: new FormControl('', [Validators.required]),
    note: new FormControl(null, [Validators.min(0), Validators.max(5)]),
  });

  constructor(
    private filmService: FilmService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {}

  onSubmit(value: FilmInterface) {
    console.log(typeof value);

    this.filmService.addFilm(value).subscribe((res) => {
      console.log(res);
      this.toastr.success('Ajout du film réussi', 'Ajout du film');
    });
    // this.onFilmsFormsChange.emit(value);
  }

  popUp(message: string): void {
    alert(message);
  }
}
