import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddOrEditFilmComponent } from './add-or-edit-film/add-or-edit-film.component';
import { AppComponent } from './app.component';
import { DialogEditFilmComponent } from './dialog-edit-film/dialog-edit-film.component';
import { ExclusiviteComponent } from './exclusivite/exclusivite.component';
import { FilmDetailsComponent } from './film-details/film-details.component';
import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MyfilmsComponent } from './myfilms/myfilms.component';
import { SallesComponent } from './salles/salles.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'my-films', component: MyfilmsComponent, canActivate: [AuthGuard] },
  { path: 'salles', component: SallesComponent, canActivate: [AuthGuard] },
  {
    path: 'exclusivite',
    component: ExclusiviteComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'film-detail/:id',
    component: FilmDetailsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'add-film',
    component: AddOrEditFilmComponent,
    canActivate: [AuthGuard],
  },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
