import { Injectable } from '@angular/core';
import { EmailValidator } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  url = 'http://localhost:3098/user';

  constructor(private http: HttpClient) {}

  login(pseudo: string, password: string): Observable<any> {
    let values = { pseudo: pseudo, password: password };
    return this.http.post<any>(this.url + '/login', values);
  }

  allUsers(): Observable<any> {
    return this.http.get<any>(this.url + '/allUsers');
  }
}
