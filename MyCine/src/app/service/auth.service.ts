import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  logged: boolean = false;
  constructor() {}

  isLoggedIn(): boolean {
    if (localStorage.getItem('logged') == 'true') {
      this.logged = true;
      return this.logged;
    }
    return this.logged;
  }
  setLogged(value: boolean): void {
    this.logged = value;
  }
}
