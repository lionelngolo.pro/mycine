import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FilmInterface } from '../interface/filmInterface';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class FilmService {
  url: string = 'http://localhost:3098/films';

  constructor(private http: HttpClient) {}

  getAllFilms(): Observable<FilmInterface> {
    return this.http.get<FilmInterface>(this.url + '/getAllFilms');
  }

  getFilm(id: number): Observable<any> {
    let values = { id: id };
    return this.http.post<any>(this.url + '/getFilm', values);
  }

  deleteFilm(id: number): Observable<any> {
    let values = { id: id };
    return this.http.post<any>(this.url + '/deleteFilm', values);
  }

  updateFilm(value: FilmInterface): Observable<any> {
    return this.http.post<any>(this.url + '/updateFilm', value);
  }

  addFilm(values: FilmInterface): Observable<any> {
    return this.http.post<any>(this.url + '/addFilm', values);
  }
}
