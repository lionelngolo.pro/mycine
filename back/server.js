var express = require("express");
var app = express();
var cors = require("cors");
var filmsRouter = require("./routes/films.routes");
var userRouter = require("./routes/user.routes");
var indexRouter = require("./routes/index.routes");
var bodyParser = require("body-parser");
var compression = require("compression");
const port = 3098;

app.use(cors());

app.use(compression());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use("/", indexRouter);
app.use("/user", userRouter);
app.use("/films", filmsRouter);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
