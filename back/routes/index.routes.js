const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  res.send("Welcome to the Backend of MyCine !");
});

module.exports = router;
