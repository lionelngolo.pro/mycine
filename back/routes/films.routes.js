const express = require("express");
const router = express.Router();
const films_controller = require("../controllers/films_controller.js");
router.get("/", (req, res) => {
  res.send("API Films  !");
});
router.post("/addFilm", films_controller.addFilms);

router.post("/deleteFilm", films_controller.deleteFilms);
router.post("/updateFilm", films_controller.updateFilms);
router.get("/getAllFilms", films_controller.getAllFilms);
router.post("/getFilm", films_controller.getAFilm);

module.exports = router;
