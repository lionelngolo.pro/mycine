const express = require("express");
const router = express.Router();
const user_controller = require("../controllers/user_controller.js");

router.get("/", (req, res) => {
  res.send("API Films USER !");
});
router.post("/login", user_controller.login);
router.get("/allUsers", user_controller.allUsers);

module.exports = router;
