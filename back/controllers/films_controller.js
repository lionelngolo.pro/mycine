var express = require("express");
var config = require("../config/config");

var con = config.connection;
//Add films
const addFilms = (req, res) => {
  let titre = req.body.titre;
  let sypnosis = req.body.sypnosis;
  let note = req.body.note;
  con.connect(function (err) {
    var sql =
      "insert into films (titre,sypnosis,note) values ('" +
      titre +
      "','" +
      sypnosis +
      "','" +
      note +
      "')";
    con.query(sql, function (err, result) {
      if (result) {
        res.send({ message: "valeur ajouté" });
      } else {
        res.send({ message: err });
      }
    });
  });
};

//delete films
const deleteFilms = (req, res) => {
  let id = req.body.id;
  con.connect(function (err) {
    var sql = "delete from films where films.id ='" + id + "' ";
    con.query(sql, function (err, result) {
      if (result) {
        res.send({ message: "Suppresion réussie" });
      } else {
        res.send(err);
      }
    });
  });
};

//update films
const updateFilms = (req, res) => {
  let id = req.body.id;
  let titre = req.body.titre;
  let sypnosis = req.body.sypnosis;
  let note = req.body.note;
  con.connect(function (err) {
    var sql =
      "update films set titre = '" +
      titre +
      "',sypnosis = '" +
      sypnosis +
      "',note = '" +
      note +
      "' where films.id = '" +
      id +
      "'";
    con.query(sql, function (err, result) {
      if (result.affectedRows) {
        res.send({ result: result });
      } else {
        res.send({ message: "update not work !!!" });
      }
    });
  });
};

//get all films
const getAllFilms = (req, res) => {
  con.connect(function (err) {
    var sql = "select * from films ";
    con.query(sql, function (err, result) {
      res.send(result);
    });
  });
};

//get one film
const getAFilm = (req, res) => {
  let id = req.body.id;
  con.connect(function (err) {
    var sql = "select * from films where films.id = '" + id + "'";
    con.query(sql, function (err, result) {
      if (result) {
        res.send({ result: result });
      } else {
        res.send({ result: err });
      }
    });
  });
};

module.exports = {
  addFilms,
  deleteFilms,
  updateFilms,
  getAllFilms,
  getAFilm,
};
